/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * 消息提醒框<br>
 * <pre>
function alert1() {
	VUI.Msg.alert('我的消息','这是一个提示信息！');
}

function confirm1() {
	VUI.Msg.confirm('确认对话框', '您想要退出该系统吗？', function(r){
		if (r){
			alert('退出')
		}else{
			alert('不退出')
		}
	});
}
function prompt1() {
	VUI.Msg.prompt('提示信息', '请输入你的姓名：', function(r,msg){
		if(r) {
			alert('你的姓名是：' + msg);
		}
	});

}
 * </pre>
 * @class VUI.Msg
 * @extends VUI.Dialog
 */
var Msg = VUI.Class('Msg',{
	/**
	 * @constructor
	 * @ignore
	 */
	init:function(opts) {
		this._super(opts);
	}
},VUI.Dialog);

var msg,alertMsg,$win,callback;
var contentId = '_msgDefWinId_';

/**
 * @ignore
 */
var getMsg = function() {
	if(!msg) {
		$('body').append($('<div id="'+contentId+'" style="min-width:200px;"></div>'));
		
		msg = new Msg({
			contentId:contentId
			,onOk:function() {
				callback && callback(true);
				this.hide();
			}
			,onCancel:function() {
				callback && callback(false);
				this.hide();
			}
		});
		
		msg.setNextDo(function(){
			this.hideBtn(1);// 隐藏取消按钮
		});
	}
	return msg;
}

/**
 * @ignore
 */
var getWin = function() {
	if(!$win) {
		$win = $('#'+ contentId);
	}
	return $win;
}

/**
 * 确认框
 * @static
 */
Msg.confirm = function(title,content,fn) {
	callback = fn;
	getMsg().setTitle(title);
	getWin().html(content || '');
	getMsg().show();
	getMsg().setNextDo(function(){
		this.showBtn(1);
	})
}

/**
 * 提醒框
 * @static
 */
Msg.alert = function(title,content,fn) {
	Msg.confirm(title,content,fn);
	getMsg().setNextDo(function(){
		this.hideBtn(1);// 隐藏取消按钮
	});
}

/**
 * 文版本提示框
 * @static
 */
Msg.prompt = function(title,content,fn) {
	content = '<div>'+content+'</div><div><input type="text" style="width:250px;"/></div>';
	Msg.confirm(title,content,function(r){
		var val = getWin().find('input').eq(0).val()
		fn(r,val);
	});
	getWin().width('auto');
}

})();