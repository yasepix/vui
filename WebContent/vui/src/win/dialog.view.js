;(function(){

/**
 * @ignore
 * @class
 */
VUI.Class('DialogView',{
	/**
	 * 
	 * @constructor
	 */
	init:function(cmp) {
		this._super(cmp);
	}
	// @override	
	,afterRender:function() {
		this._super();
		this.initEvent();
	}
	,initEvent:function() {
		var that = this;
		this.getWin().find('input,select,button').on('keydown',function(e){
			if(e.keyCode == 13) { // 处理回车
				var onOk = that.opt('onOk');
			 	onOk && onOk.call(that);
			}
		});
	}
},VUI.WinView);

})();