/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * @ignore
 * @class
 */
VUI.Class('Taskbar',{
	/**
	 * 构造函数
	 * @constructor
	 */
	init:function() {
		this.wins = [];
	}
	,add:function(winView) {
		this.wins.push(winView);
		this.calcPosition();
	}
	,del:function(winView) {
		this.remove(winView);
		this.calcPosition();
	}
	,calcPosition:function() {
		var nextOffsetLeft = 0;
		var windowHeight = $(window).height();
		
		for(var i=0, len=this.wins.length;i<len; i++) {
			var winView = this.wins[i];
			if(winView) {
				var titleSize = this.buildTitleSize(winView);
				
				winView.animateMove(nextOffsetLeft,windowHeight - titleSize.height);
				nextOffsetLeft += titleSize.width;
			}			
		}
	}
	,indexOf:function(winView) {
		for (var i = 0,len=this.wins.length; i < len; i++) {
			if (this.wins[i] == winView) {
				return i;
			}
		}
		return -1;
	}
	,remove:function(winView) {
		var index = this.indexOf(winView);
		if (index > -1) {
			this.wins.splice(index, 1);
		}
	}
	// 计算标题宽度
	,buildTitleSize:function(winView) {
		var titleBarClass = 'pui-dialog-titlebar';
		var $titleBar = winView.getWin().find('.pui-dialog-titlebar').eq(0);
		
		var titleTxtWidth = $titleBar.find('.pui-dialog-title').innerWidth();
		var titleTxtHeight = $titleBar.innerHeight();
		
		var tagAWidth = 0;
		var space = 40;
		
		var $tagA = $titleBar.find('a');
		$tagA.each(function(){
			tagAWidth +=$(this).innerWidth();
		});
		
		var finalWith = titleTxtWidth + space + tagAWidth;
		
		return {width:finalWith,height:titleTxtHeight};
	}
});

})();