/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){


var datepick = null;

var calendar = new VUI.Calendar({
	autoRender:false
	,showFoot:true
	,autoHide:true
	,position:'absolute'
	,zindex:999
	,clickHandler:function(data) {
		datepick.fire('Click',data);
	}
	,cancelHandler:function(data) {
		datepick.fire('Cancel',data);
	}
});

calendar.render(document.body);

// 点击其它地方隐藏
$(document).click(function(e){
	if(calendar && calendar.visiable()) {
		var $target = $(e.target); 
		var $input = datepick.$target;
		
		if($target.get(0) == $input.get(0)) {
			return;
		}
		
		if($target.closest(calendar.view.getWraper()).length == 0){ 
			calendar.hide();
		}  
	}
})


/**
 * 日期选择器<br>
 * <pre>
new VUI.DatePick({
	targetId:'pick'
});

new VUI.DatePick({
	targetId:'pick2'
});
 * </pre>
 * @class VUI.DatePick
 * @extends VUI.Component
 */
VUI.Class('DatePick',{
	// 默认属性
	OPTS:{
		/**
		 * @cfg {String} targetId 目标元素的ID
		 */
		targetId:''
		,offsetX:0
		,offsetY:23
		,onClick:function(data) {
			this.$target.val && this.$target.val(data.y + '-' + data.m + '-' + data.d);
			this.hideCalendar();
		}
		,onCancel:function() {
			this.$target.val && this.$target.val('');
			this.hideCalendar();
		}
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
		this.bind();
	}
	/**
	 * @private
	 */
	,bind:function() {
		var that = this;
		this.calendar = calendar;
		var targetId = this.opt('targetId');
		
		this.$target = $('#'+targetId).click(function(){
			datepick = that;
			that.showCalendar();
		}).css({cursor:'pointer'})
		.attr('placeholder','点击选择日期');
	}
	/**
	 * @private
	 */
	,showCalendar:function() {
		this._setPosition(this.$target);
		this.calendar.showCalendar(this.$target.val());
	}
	/**
	 * @private
	 */
	,hideCalendar:function() {
		this.calendar.hide();
	}
	,_setPosition:function($target) {
		var offset = $target.offset();
	    var $calDiv = this.calendar.view.getWraper();
	    $calDiv.css({
	    	left : (offset.left + this.opt('offsetX')+'px')
	    	,top : (offset.top + this.opt('offsetY')+'px')
	    });
	    
	}
},VUI.Common);

})();