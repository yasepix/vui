/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */
/**
 * @ignore
 */
VUI.Config = {
	Grid:{
		// 发送请求的页索引属性名
		requestPageIndexName:'page'
		//发送请求的每页大小属性名
		,requestPageSizeName:'rows'
		// 发送请求的排序字段属性名
		,requestSortName:'sort'
		// 发送请求的排序规则属性名
		,requestOrderName:'order'
		// 服务器端返回json数据的页索引标识
		,serverPageIndexName:'pageIndex'
		// 服务器端返回json数据的页大小标识
		,serverPageSizeName:'pageSize'
		// 服务器端返回json数据的总记录数标识
		,serverTotalName:'total'
		// 服务器端返回json数据的rows标识
		,serverRowsName:'rows'
	}
	,Win:{
		// 起始值
		zIndex:100
	}
	,Validate:{
		errorClass:'error'
		,successClass:'success'
	}
	,Tree:{
		level:4 // 层级
	}
	,DatePick:{
		weekTextItems:['日','一','二','三','四','五','六']
	}
}