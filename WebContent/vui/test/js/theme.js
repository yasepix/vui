// 加载主题,仅做demo使用
;(function() {
	var search = location.search;
	var themeName = getParamMap()['theme'];
	if(themeName){
		$('#v_theme').attr('href','../../src/css/theme/'+themeName+'/theme.css');
	}
	
	function getParamMap() {
		var map = {};
		var query = location.search; 
		
		if(query && query.length>=1) {
			query = query.substring(1);
			var pairs = query.split("&");
			for(var i = 0; i < pairs.length; i++) { 
				var pos = pairs[i].indexOf('='); 
				if (pos == -1) continue; 
				var argname = pairs[i].substring(0,pos); 
				var value = pairs[i].substring(pos+1); 
				value = decodeURIComponent(value);
				map[argname] = value;
			}
		}
			
		return map;
	}
	
	window.setTheme = function(themeName,themePath) {
		themePath = themePath || '../../src/css/theme/';
		if(themeName){
			var themeLink = document.getElementById('v_theme');
			themeLink.href = themePath + themeName+'/theme.css';
		}
	}
})();